package de.htwg.scm
import com.github.weisj.darklaf.LafManager
import com.github.weisj.darklaf.LafManager.getPreferredThemeStyle
import com.github.weisj.darklaf.theme.{DarculaTheme, IntelliJTheme}

import scala.swing._

object Main {
  def setPreferredTheme(): Unit = {
    val x = LafManager.themeForPreferredStyle(getPreferredThemeStyle);
    LafManager.install(x);
  }
  def setLightTheme(): Unit = {
    LafManager.setTheme(new IntelliJTheme());
    LafManager.install();
  }
  def setDarkTheme(): Unit = {
    LafManager.setTheme(new DarculaTheme());
    LafManager.install();
  }
  def main(args: Array[String]): Unit = {
    setPreferredTheme();

    new Frame {
      title = "Hello world"

      contents = new FlowPanel {
        contents += new Label("Launch rainbows:")
        contents += new Button("Click me") {
          reactions += {
            case event.ButtonClicked(_) =>
              LafManager.setTheme(new IntelliJTheme());
              LafManager.install();
          }
        }
      }

      pack()
      centerOnScreen()
      open()
    }
  }
}